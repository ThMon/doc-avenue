import React from 'react';
import logo from './logo.svg';
import './App.css';
import Home from './Components/Home';
import Basket from './Components/Basket';
import { NavLink, Route } from 'react-router-dom';
import { connect } from 'react-redux'

class App extends React.Component
{
    render() {
      let products;
      
      if(this.props.basketItems.length >0 ) {
        products = <span>{this.props.basketItems.length}</span>
      }

      return (
        <main >
          <nav id="menu">
            <NavLink className="nav-title" to="/">
              home
            </NavLink>
            <NavLink className="nav-title" to="/basket">
              panier 
            </NavLink>
            { products }
          </nav>

          <section id="contents">
            <Route exact path="/" component={ Home } />
            <Route exact path="/basket" component={ Basket } />
          </section>
        </main>
      );
    }
}

const mapStateToProps = state => {
  return {
    basketItems: state.basket.basketItems
  }
}


export default connect(mapStateToProps)(App) 
