import React from 'react';
import { connect } from 'react-redux'
import BasketItem from './BasketItem'
import {removeToBasket} from '../redux/actions/basketActions'

class Basket extends React.Component {
  loadBasket() {
    if (this.props.basketItems.length > 0) {
      return this.props.basketItems.map((item) => {
        return <BasketItem item={item}  key={item.id} removeMovieToBasket={this._removeMovieToBasket}/>
    })
    } else {
      return <p>Votre panier est vide</p>
    }
  }

  _removeMovieToBasket = (product) => {
      this.props.dispatch(removeToBasket(product))
  }

  render() {
    return (
      <div>
        <h1>VOTRE PANIER</h1>
        {this.loadBasket()}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    basketItems: state.basket.basketItems
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Basket); 