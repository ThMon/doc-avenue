import React from 'react';

class BasketItem extends React.Component {

	render() {
		return (
			<li className="basket-item">
				<img src={this.props.item.thumbnailUrl}/>
				<div className="info">
					<h3>Nom: {this.props.item.title}</h3>
					<p>Quantité: {this.props.item.quantity}</p>
					<div className="deleteItem" onClick={()=> {this.props.removeMovieToBasket(this.props.item)}}>Supprimer</div>
				</div>
			</li>
		);
	}
}

export default BasketItem;