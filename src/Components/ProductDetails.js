import React from 'react';

class ProductDetails extends React.Component {

	render() {
		return (
			<article className="product-item">
				<img src={this.props.item.thumbnailUrl} className="productImage"/>
				<h3>{this.props.item.title.substr(0, 20)}...</h3>
				<div className="add-basket" onClick={()=> {this.props.addMovieToBasket(this.props.index)}}><i className="fas fa-plus-circle"></i></div>
			</article>
		);
	}
}

export default ProductDetails;