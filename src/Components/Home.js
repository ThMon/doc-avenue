import React from 'react';
import { getAllProducts } from '../API/placeholder';
import ProductDetails from './ProductDetails';
import { connect } from 'react-redux'
import { addToBasket } from '../redux/actions/basketActions'


class Home extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        items: [],
        max: 15,
        min: 0
      };

  }

  componentDidMount() {
    this.loadProducts();
  }

  loadProducts() {
    getAllProducts()
      .then((res)=> {
        this.setState({items: res});
      })
      .catch((err)=>{
        console.log(err);
      })
  }

  nextProducts(e) {
    e.preventDefault();
    if(this.state.max < this.state.items.length) {
      this.setState({
        max : this.state.max += 15,
        min: this.state.min += 15,
      })
    }
  }

  prevProducts(e) {
    e.preventDefault();
    if(this.state.min >= 15) {
      this.setState({
        max : this.state.max -= 15,
        min: this.state.min -= 15,
      })
  }
  }

  displayProducts() {
    return this.state.items.map((item, index)=> {
      if(index < this.state.max && index >=this.state.min) {
        return <ProductDetails key={index} index={index} item={item} addMovieToBasket = {this._addMovieToBasket}/>
      }
      
    });
  }

  _addMovieToBasket = (index) => {
    this.props.dispatch(addToBasket(this.state.items[index]))
  }

  render() {
    return (
      <div className="container">
        <h1>NOS PRODUITS</h1>
        {this.displayProducts()}
        <div className="direction">
          <span onClick={(e)=>{this.prevProducts(e)}}><i className="fas fa-arrow-left"></i></span>
          <span onClick={(e)=>{this.nextProducts(e)}}><i className="fas fa-arrow-right"></i></span>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    basketItems: state.basket.basketItems
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    dispatch: (action) => { dispatch(action) }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home) 