const initialState = { basketItems: [] }

function basketReducer(state = initialState, action) {
  let nextState
  let updatedBasket;
  let updatedItemIndex;
  
  switch (action.type) {
    case 'ADD_BASKET':

      let newItem = action.value;
      newItem.quantity = 1;

    	let basketIndex = state.basketItems.findIndex(item => item.id === newItem.id)
      if(basketIndex !== -1) {

        updatedBasket = [...state.basketItems];

        const incrementedItem = {
                ...updatedBasket[basketIndex]
        };

        incrementedItem.quantity++;
        updatedBasket[basketIndex] = incrementedItem;

        nextState = {
          ...state, 
          basketItems: updatedBasket
        }

      } else {
        nextState = {
          ...state,
          basketItems: [...state.basketItems, newItem]
        }
      }

     	return nextState || state

    case 'REMOVE_BASKET':
      nextState = {
          ...state,
          basketItems: state.basketItems.filter( (item) =>  item.id !== action.value.id)
      }
      
      return nextState || state
      
    default:
      return state
  }
}

export default basketReducer;