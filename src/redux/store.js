import { combineReducers } from 'redux';
import basketReducer from './reducers/basketReducer'

const Store = combineReducers({ basket: basketReducer });

export default Store;