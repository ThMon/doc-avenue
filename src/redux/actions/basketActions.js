export function addToBasket(product) {
      const action = { type: "ADD_BASKET", value: product };
      return action;
}

export function removeToBasket(product) {
      const action = { type: "REMOVE_BASKET", value: product };
      return action;
}

